# Arch-script
## Данный скрипт лучше всего совместим с [Arch Linux GUI](https://archlinuxgui.in/)
Скрипт для автоматизации рутины после установки Arch Linux

Для запуска скрипта введите в терминале ниже команду:

- sudo pacman -Sy --needed git && git clone https://codeberg.org/Lintech/Arch-script && cd Arch-script && sh arch.sh